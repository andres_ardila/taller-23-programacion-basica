print("Bienvenido, vamos a aprender a usar el método strip() de la clase str:")

cadena=input("Por favor ingrese una cadena de texto con espacios vacíos en ambos lados:")

print("El método strip(), nos ayuda a quitar los caracteres de espacio vacío del lado derecho y el izquierdo de una cadena")

print("Aplicando strip() al texto:",cadena.strip())