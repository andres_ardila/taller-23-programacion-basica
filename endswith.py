print("Bienvenido, vamos a aprender a usar el método endswith() de la clase str,\nCon endswith() puedo verificar si la cadena de texto finaliza por un conjunto de carácteres determinados")

cadena=input("Ingrese una cadena de texto:")
finalizaPor=input("Ingrese una letra o conjunto de letras que estarían en el final:")

if cadena.endswith(finalizaPor):
    print("La cadena finaliza por:",finalizaPor)
else:
    print("La cadena no finaliza por:",finalizaPor)