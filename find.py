print("Bienvenido, vamos a aprender a usar el método find() de la clase str,\nCon find() puedo buscar algo dentro de esa cadena de texto y me va a devolver el indice en el cual está lo que buscamos.\nSi lo que queremos buscar no se encuentra dentro de la cadena de texto nos va a devolver -1")

cadena=input("Ingrese una cadena de texto:")
encuentra=input("Ingrese una letra o conjunto de letras que desea buscar:")

print("Para facilitar nuestro trabajo convertimos lo que está dentro de la cadena de texto a mayúsculas o minúculas")

cadena1=cadena.upper()
encuentra1=encuentra.upper()

print(cadena1.find(encuentra1))
