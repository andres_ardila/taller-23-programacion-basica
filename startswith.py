print("Bienvenido, vamos a aprender a usar el método sartswith() de la clase str,\nCon startswith() puedo verificar si la cadena de texto empieza por un conjunto de carácteres determinados")

cadena=input("Ingrese una cadena de texto:")
empiezaPor=input("Ingrese una letra o conjunto de letras que estarían en el inicio:")

if cadena.startswith(empiezaPor):
    print("La cadena empieza por:",empiezaPor)
else:
    print("La cadena no empieza por:",empiezaPor)